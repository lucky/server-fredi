const mongoose = require("mongoose")
const Schema = mongoose.Schema;


let tresorierSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  nom: String,
  prenom: String,
  date: {
    type: Date,
    default: new Date()
  }



})



let Tresorier = mongoose.model("Tresorier", tresorierSchema)
exports.Tresorier = Tresorier;
