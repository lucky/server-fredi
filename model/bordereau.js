const mongoose = require("mongoose")
const Schema = mongoose.Schema;


let bordereauSchema = new Schema({
  annee: {
    type: Number,
    required: true,
  },
  frais: [],

  user: {
    type: Schema.Types.ObjectId,
    ref: "User"
  },

  validated: {
    type: Boolean,
    default: false
  }



})



let Bordereau = mongoose.model("Bordereau", bordereauSchema)
exports.Bordereau = Bordereau;
