const mongoose = require("mongoose")
const Schema = mongoose.Schema;


let userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  nom: String,
  prenom: String,
  dateNaissance: Date,
  numLicence: {
    type: Number,
    required: true,
    unique: true
  },
  numMobile: Number,
  //date d'inscription du user
  // date: {
  //   type: Date,
  //   default: new Date()
  // }


})



let User = mongoose.model("User", userSchema)
exports.User = User;
