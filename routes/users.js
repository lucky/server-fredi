const express = require('express');
const router = express.Router();
const mongoose = require("mongoose")
const User = require("../model/user").User; // on importe l'architecture de user
const Bordereau = require("../model/bordereau").Bordereau;
const jwt = require("jwt-simple");
const json2xls = require('json2xls');
const fs = require("fs");
const nodeExcel = require('excel-export');
const xml2json = require('xml2json');
const xml2jsParseString = require('xml2js').parseString;

var json = {
    foo: 'bar',
    qux: 'moo',
    poo: 123,
    stux: new Date()
}


// la clé pour coder/décoder le token
const key = "xTUVdgQIeW";

// fonction checkUser qui sera executé lors des requetes nécessitant la connexion

function checkUser(token){
  return new Promise(function(resolve, reject) {
    let decoded = jwt.decode(token, key);
    User.findById(decoded.id, (err, user)=>{
      if(user){

        resolve({
          success: true,
          user: user
        })
      }else{
        reject({
          success: false
        })
      }
    });
  });

}

// router pour le login
router.post('/login', function(req, res, next) {
  // si le mail et la password existent
  if(req.body.email && req.body.password){
    User.findOne({email: req.body.email}, (err, user)=>{
      // si le user avec cet eamil existe on vérifie le mdp

      if(!user){
        res.json({
          success: false,
          message: "Cet utilisateur n'existe pas"
        })
      }else{

        if(user.password === req.body.password){
          let token = jwt.encode({
            id: user._id
          }, key) // on hash l'id du user avec la clé définie précedemment
          // et on envoie ce token au côté client.
          user.password = null;
          res.json({
            success: true,
            user: user,
            token: token,
            message: "Connexion avec succès"
          })
        }else{
          res.json({
            success: false,
            message: "Mot de passe erroné."
          })
        }


      }
    })
  }else{
    res.send(401);
  }

});


// router pour l'inscription
router.put('/register', (req, res, next) =>{

  let u = new User();
  console.log("/////")
  u.email = req.body.email;
  u.password = req.body.password;
  u.nom = req.body.nom;
  u.prenom = req.body.prenom;
  u.numLicence = parseInt(req.body.numLicence);
  u.numMobile = parseInt(req.body.numMobile);

  // maintenant on convertit la date en objet Date
  let spliter = req.body.dateNaissance.split("/")
  u.date = new Date(spliter[2], spliter[1], spliter[3])


  u.save(function (err, user){
    if(err){
      // s'il y a une erreur, on regarde d'abord si elle liée à un duplicate_key
      if(err.code == 11000){
        res.json({
          success: false,
          message: "L'utilisateur existe déjà, veuillez vous connecter."
        })
      } else {
        console.log(err)
        // si la nature de l'err est différente, envoyer juste le message d'erreur
        res.json({
          success: false,
          message: err.errmsg
        })
      }


    }else{
      console.log(user)
      res.json({
        success: true,
        user: user,
        message: "L'utilisateur a bien été créé."
      })

    }

  })

});

// fonction pour récupérer un bordereau existant
// new fonction

router.post("/getBordereaux", (req, res) => {
  if(req.body.token){
    checkUser(req.body.token).then(check=>{
        if (check.success){
          console.log(check)
          Bordereau.find({user: check.user._id}, (err, b)=>{
            console.log(b)
            res.json({
              success: true,
              bordereaux: b
            })
          })


        } else { res.json({
          success : false,
          message : "erreur d'identification"
        })
      }
    })
  } else {
    res.json({
      success : false,
      message : "vous devez être connecté"
    })

  }
})

// fonction pour créer un bordereau

router.put("/addBordereau", (req, res) => {
  if(req.body.token){
    // vérifions avec la fonction checkUser si le user est connecté
    checkUser(req.body.token).then(check=>{
      if(check.success){
        let b = new Bordereau()
        b.annee = req.body.bordereau.annee;
        b.frais = req.body.bordereau.frais;
        b.user = check.user._id;
        b.save((err, bordereau)=>{
          console.log(err)
          res.json({
            success: true,
            bordereau: bordereau,
            message: "Le bordereau a bien été créé."
          })
        })
      }else{
        res.json({
          success : false,
          message : "vous devez être connecté"
        })
      }
    })


  } else {
    res.json({
      success : false,
      message : "vous devez être connecté"
    })
  }

})


//route pour supprimer les données utilisateur

router.post("/supprimerUser", function (req, res){
  if (req.body.token){
    checkUser(req.body.token).then(check=>{
      if(check.success){
        User.findById(check.user._id, (err, user)=>{

            if(user._id.equals(check.user._id)){
              user.remove((err, success)=>{
                console.log(success)
                console.log("utilisateur supprimé")
                res.json({
                  success: true,
                  message: "l'utilisateur a bien été supprimé"
                })
              })
            }

          })
        }
      })
    } else {
      res.json({
        success: true,
        message: "what's wrong"
      })
    }

  })


// route pour vérifier le usersRouter
router.post("/checkUser", function (req, res){
  if(req.body.token){
    let token = req.body.token;

    let decoded = jwt.decode(token, key);

    User.findById(decoded.id, (err, user)=>{
      if(user){
        user.password = null;
        user._id = null;
        res.json({
          success: true,
          user: user
        })
      }else{
        res.json({
          success: false
        })
      }
    });

  }else{
    res.send(401)
  }
});

// router pour supprimer un bordereau

router.post("/deleteBordereau", (req, res) =>{
  console.log(req.body)
  if(req.body.token){
    checkUser(req.body.token).then(check=>{
      if(check.success){
        // l'utilisateur est bien connecté. Vérifions s'il essaye de supprimer son propre bordereau et pas celui de qqun d'autre
        Bordereau.findById(req.body.bordereau._id, (err, bordereau)=>{
          if(bordereau.user._id.equals(check.user._id)){
            bordereau.remove((err, success)=>{
              res.json({
                success: true,
                message: "Le bordereau vient d'être supprimé."
              })
            })
          } else {
            res.json({
              success: false,
              message: "Vous n'êtes pas le propriétaire de ce bordereau"
            })
          }
        })
      } else {
        console.log("test")
        res.json({
          success: false,
          message: "Vous devez être connecté pour supprimer"
        })
      }
    })
  } else {
    console.log("fuck")
    res.json({
      success: false,
      message: "Vous devez être connecté pour supprimer"
    })
  }

})

router.get("/exportBordereau/:id/:token", (req, res)=>{

  Bordereau.findById(req.params.id, (err, bordereau)=>{
    if(!err){
      checkUser(req.params.token).then(check=>{

        if(check.success){
          User.findById(check.user._id, (err, user)=>{

              if(bordereau.user.equals(check.user._id)){


                let frais = bordereau.frais;
                let xls = json2xls(frais);
                fs.writeFileSync('./exported/test.xlsx', xls, 'binary');
                let filebordereau = './exported/test.xlsx';
                res.download(filebordereau, "filebordereau"); // Set disposition and send it.


                // var fs = require('fs')
                //
                // var str = fs.readFileSync('style.xml').toString()
                //
                // str = str.replace('Année civile 2009', 'Année civile '+bordereau.annee);
                // //
                // // var jsonfile = xml2json.toJson(str);
                // // let xlsfile = json2xls(jsonfile);
                //
                // xml2jsParseString(str, function(err, result){
                //   jsonTextFromXML = JSON.stringify(result);
                //   console.log("result", jsonTextFromXML);
                // })
                //
                // fs.writeFileSync('./exported/notedefrais.xml', str);
                // let file = './exported/notedefrais.xml';
                // res.download(file);


              }

          })


        } else {
          res.json({
            success: true,
            message: "what's wrong"
          })
        }
      })

    }else{
      res.send(400)
    }
  })

})





module.exports = router;
