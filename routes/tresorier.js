const express = require('express');
const router = express.Router();
const mongoose = require("mongoose")
const User = require("../model/user").User; // on importe l'architecture de user
const Bordereau = require("../model/bordereau").Bordereau;
const Tresorier = require("../model/tresorier").Tresorier;
const jwt = require("jwt-simple");
const json2xls = require('json2xls');
const fs = require("fs")

// la clé pour coder/décoder le token
const key = "xTUVdgQIeW";

// fonction pour vérifier  si le trésorier est bien connecté

function checkTresorier(token){
  return new Promise(function(resolve, reject) {
    let decoded = jwt.decode(token, key);
    Tresorier.findById(decoded.id, (err, tresorier)=>{
      if(tresorier){
        resolve({
          success: true,
          tresorier: tresorier
        })
      }else{
        reject({
          success: false
        })
      }
    });
  });

}


// route pour vérifier si le tresorier est connecté ou passe
// route pour vérifier le usersRouter
router.post("/checkTresorier", function (req, res){
  if(req.body.token){
    let token = req.body.token;

    let decoded = jwt.decode(token, key);

    Tresorier.findById(decoded.id, (err, tresorier)=>{
      if(tresorier){
        tresorier.password = null;
        tresorier._id = null;
        res.json({
          success: true,
          tresorier: tresorier
        })
      }else{
        res.json({
          success: false
        })
      }
    });

  }else{
    res.send(401)
  }
});

// router pour le login
router.post('/login', function(req, res, next) {
  // si le mail et la password existent
  if(req.body.email && req.body.password){
    Tresorier.findOne({email: req.body.email}, (err, tresorier)=>{
      // si le user avec cet eamil existe on vérifie le mdp

      if(!tresorier){
        res.json({
          success: false,
          message: "Ce trésorier n'existe pas"
        })
      }else{

        if(tresorier.password === req.body.password){
          let token = jwt.encode({
            id: tresorier._id
          }, key) // on hash l'id du user avec la clé définie précedemment
          // et on envoie ce token au côté client.
          tresorier.password = null;
          res.json({
            success: true,
            tresorier: tresorier,
            token: token,
            message: "Connexion avec succès"
          })
        }else{
          res.json({
            success: false,
            message: "Mot de passe erroné."
          })
        }


      }
    })
  }else{
    res.send(401);
  }

});


router.post('/validerBordereau', (req, res)=>{
  checkTresorier(req.body.token).then(check =>{
    if(check.success){
      Bordereau.updateOne({_id: req.body.bordereau._id},{validated:true}, (err, bordereau)=>{
        res.json({
          succes:true,
          message:"le bordereau a bien été validé",
          bordereau:bordereau
        })
      })
    } else {
      res.json({
        success:false,
        message:"vous n'êtes pas connecté"
      })
    }
  })
})


router.post('/getUser', (req, res)=> {
  checkTresorier(req.body.token).then(check =>{
    if(check.success){
      User.findOne({numLicence: req.body.numLicence}).exec((err, user)=>{
        if(user){
          Bordereau.find({user: user._id}, (err, bordereaux)=>{
            user.password = null
            user._id = null


            res.json({
              success: true,
              user: user,
              bordereaux: bordereaux
            })
          })


        }else{
          res.json({
            success: false,
            message: 'pas de numéro de licence associé'
          })
        }
      })
    }
  })
  // User.findOne({
  //   numLicence:
  // })


})



module.exports = router;
