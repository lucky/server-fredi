const MJ_APIKEY_PUBLIC='7c69dbf906a6e514c3be913d77c2d4e8'
const MJ_APIKEY_PRIVATE='f26d3271e7a260a3f9e30b295d65d425'

/**
 *
 * Run:
 *
 */
const mailjet = require ('node-mailjet')
    .connect(MJ_APIKEY_PUBLIC, MJ_APIKEY_PRIVATE)
const request = mailjet
    .post("send", {'version': 'v3.1'})
    .request({
        "Messages":[
                {
                        "From": {
                                "Email": "lucasflorentin@outlook.fr",
                                "Name": "Me"
                        },
                        "To": [
                                {
                                        "Email": "lucas.florentin1@epsi.fr",
                                        "Name": "You"
                                }
                        ],
                        "Subject": "My first Mailjet Email!",
                        "TextPart": "Greetings from Mailjet!",
                        "HTMLPart": "<h3>Dear passenger 1, welcome to <a href=\"https://www.mailjet.com/\">Mailjet</a>!</h3><br />May the delivery force be with you!"
                }
        ]
    })
request    .then((result) => {
        console.log(result.body)
    })
    .catch((err) => {
        console.log(err.statusCode)
    })
