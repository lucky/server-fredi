let model = require("./model/model")
let Tresorier = require("./model/tresorier").Tresorier



let tresoriers = [
  {
    nom: "Florentin",
    prenom: "Lucas",
    email: "admin@test.com",
    password: "mdp",
    club: "Manchester"
  },{
    nom: "Dupont",
    prenom: "Nicolad",
    email: "nicolas@test.com",
    password: "mdp",
    club: "Juventus"
  }
]


model.connect(()=>{
  for( tresorier of tresoriers){
    let t = new Tresorier(tresorier)
    t.save()
  }
})
